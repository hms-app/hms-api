import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from 'cors';
import config from './config';
import rootRoute from './routes/root';
import measurements from './routes/measurements';
import userAuth from './routes/userAuth';

const app = express();
// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

mongoose.connect(
  "mongodb://" + config.dbhost + ":27017/" + config.db,
  err => {
    if (err)
      throw err;
  }
);

const db = mongoose.connection;
const router = express.Router();

router.use((req, res, next) => {
    if(config.isDev) 
      console.log( req.method + ':', req.originalUrl );
    next();
});

router.route('/')
  .get(rootRoute.get);

router.route('/measurements')
  .post(measurements.post)
  .get(measurements.get);

router.route('/users')
  .post(userAuth.post);

router.route('/sessions/create')
  .post(userAuth.sessionPost);

app.use(cors());
app.use('/.api', router);

app.listen(config.port);
console.log('API listening on port ' + config.port);
