const config = {
  db: "test2",
  dbhost: "localhost",
  port: process.env.PORT || 7559,
  isDev: process.env.NODE_ENV === 'development' ? true : false,
  secret: "ngEurope rocks!"
};

export default config;
