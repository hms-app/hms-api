import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const MeasurementSchema = new Schema({
  _id: false,
  t: Number,
  v: Number
});

const SensorSchema = new Schema({
  _id: { type: String, required: true },
  unit: String,
  measurements: [ MeasurementSchema ]
});

export default mongoose.model('Sensor', SensorSchema, 'Sensors');
