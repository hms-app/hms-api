var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var MeasurementSchema = new Schema({
  _id: false,
  x: Number,
  y: Number
});

var SensorSchema = new Schema({
  _id: { type: String, required: true },
  unit: String,
  measurements: [ MeasurementSchema ]
});


//MeasurementSchema.plugin(AutoIncrement);
module.exports = mongoose.model('Sensor', SensorSchema, 'Sensors');
