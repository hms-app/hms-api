import async from 'async';
import Sensor from '../db/models/Sensor';

const post = (req, res) => {
  const r = req.body;
  let i = 0;

  // Async loop required to successfully push measurements
  async.whilst(
    () =>  { return i < r.measurements.length; },
    (callback) => {
      console.log(r.measurements[i]);
      Sensor.findByIdAndUpdate(
        r.measurements[i][0],
        {
          $push: { measurements: { t: r.timestamp, v: r.measurements[i][1] } },
          $set: { unit: r.measurements[i][2] }
        },
        { upsert: true, new: true, safe: true },
        (err, result) => {
          if(err) console.log(err);
          i++;
          callback(err,result);
        }
      ); 
    },
    (err, end) => {
      if(err)
        console.log(err);
      if(end)
        res.status(201).json({msg: 'Measurements updated'} );
  });
};

const get = (req, res) => {
  Sensor.find((err, measurements) => {
    if (err)
      res.send(err);
    res.json(measurements);
  });
};

export default {
  get,
  post
}
